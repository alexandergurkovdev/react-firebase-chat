import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";
import { Form, Col, Button } from "react-bootstrap";
import RouterLink from "../../components/ui/router-link";
import Loader from "../../components/ui/loader";
import { sendPasswordRecover, cleanUp } from "../../store/actions/authActions";

const RecoverySchema = Yup.object().shape({
  email: Yup.string()
    .email("Введите корректный email")
    .required("Поле email обязательно к заполнению"),
});

const ForgotPassword = ({ loading, sendPasswordRecover, cleanUp }) => {
  useEffect(() => {
    return () => {
      cleanUp();
    };
  }, [cleanUp]);

  return (
    <Formik
      initialValues={{
        email: "",
      }}
      validationSchema={RecoverySchema}
      onSubmit={async (values, { setSubmitting }) => {
        await sendPasswordRecover(values);
        setSubmitting(false);
      }}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        errors,
        isSubmiting,
        isValid,
      }) => (
        <Form className="auth-form" noValidate onSubmit={handleSubmit}>
          {loading ? <Loader bgShow /> : null}
          <Form.Row>
            <Form.Group as={Col} md="12" controlId="validationFormik01">
              <Form.Control
                type="email"
                name="email"
                placeholder="Email"
                value={values.email}
                onChange={handleChange}
                isInvalid={!!errors.email}
              />
              <Form.Control.Feedback type="invalid">
                {errors.email}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Button type="submit" disabled={!isValid || isSubmiting}>
                Восстановить пароль
              </Button>
              <RouterLink link="/login">К странице входа</RouterLink>
            </Form.Group>
          </Form.Row>
        </Form>
      )}
    </Formik>
  );
};

const mapStateToProps = ({ auth: { loading } }) => ({
  loading,
});

const mapDispatchToProps = {
  sendPasswordRecover,
  cleanUp,
};

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
