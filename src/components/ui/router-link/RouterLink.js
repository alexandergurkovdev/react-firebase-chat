import React from "react";
import { Link } from "react-router-dom";

const RouterLink = ({ label, link, children }) => {
  return (
    <div className="router-link d-block">
      {label}
      <Link to={link}>{children}</Link>
    </div>
  );
};

export default RouterLink;
