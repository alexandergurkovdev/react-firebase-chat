import firebase from "firebase/app";
import * as actions from "./actionTypes";
import { showToast } from "../../utils/toast";

// Get users action creator
const getUsers = () => async (dispatch) => {
  dispatch({ type: actions.FETCH_USERS_START });
  try {
    const currentUser = firebase.auth().currentUser;

    await firebase
      .database()
      .ref("users")
      .on("child_added", (snapshot) => {
        if (currentUser.uid !== snapshot.key) {
          let user = snapshot.val();
          user["uid"] = snapshot.key;
          user["status"] = "offline";

          dispatch({ type: actions.FETCH_USERS_SUCCESS, payload: user });
        }
      });

    await firebase
      .database()
      .ref(".info/connected")
      .on("value", async (snapshot) => {
        if (snapshot.val() === true && currentUser) {
          let ref = await firebase
            .database()
            .ref("presence")
            .child(currentUser.uid);

          ref.set(true);
          ref.onDisconnect().remove();

          window.onunload = () => {
            ref.remove();
          };
        }
      });
  } catch (error) {
    dispatch({ type: actions.FETCH_USERS_FAILED });
    showToast(true, null, error.message);
  }
  dispatch({ type: actions.FETCH_USERS_END });
};

// Listener user statuses action creator
const listenUerStatuses = () => (dispatch) => {
  const presenceRef = firebase.database().ref("presence");

  const currentUser = firebase.auth().currentUser;

  presenceRef.on("child_added", (snapshot) => {
    if (currentUser.uid !== snapshot.key) {
      dispatch({
        type: actions.ADD_STATUS_TO_USER,
        payload: { userId: snapshot.key },
      });
    }
  });

  presenceRef.on("child_removed", (snapshot) => {
    if (currentUser.uid !== snapshot.key) {
      dispatch({
        type: actions.ADD_STATUS_TO_USER,
        payload: { userId: snapshot.key, connected: false },
      });
    }
  });
};

export { getUsers, listenUerStatuses };
