import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Switch, Route, Redirect } from "react-router-dom";
import { MainLayout, AuthLayout } from "./layouts";
import { Login, SignUp, ForgotPassword } from "./pages/auth";
import Home from "./pages/home";
import Chat from "./containers/chat";
import { checkCurrentUser } from "./store/actions/authActions";
import { ToastContainer } from "react-toastify";

const App = ({ currentUser, checkCurrentUser }) => {
  useEffect(() => {
    checkCurrentUser();
  }, []);

  let routes;

  if (currentUser) {
    routes = (
      <MainLayout>
        <Route path="/" exact component={Home} />
        <Route path="/chat/:chatId" component={Chat} />
        <Redirect to="/" />
      </MainLayout>
    );
  } else {
    routes = (
      <AuthLayout>
        <Route path="/login" component={Login} />
        <Route path="/sign-up" component={SignUp} />
        <Route path="/recovery" component={ForgotPassword} />
        <Redirect to="/login" />
      </AuthLayout>
    );
  }

  return (
    <div className="app">
      <Switch>{routes}</Switch>
      <ToastContainer />
    </div>
  );
};

const mapStateToProps = ({ auth: { currentUser } }) => {
  return { currentUser };
};

const mapDispatchToProps = {
  checkCurrentUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
