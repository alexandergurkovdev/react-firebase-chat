import firebase from "firebase/app";
import * as actions from "./actionTypes";
import { showToast } from "../../utils/toast";

const checkCurrentUser = () => (dispatch) => {
  firebase.auth().onAuthStateChanged((user) => {
    dispatch({ type: actions.CHECK_CURRENT_USER, payload: user });
  });
};

// Sign up action creator
const signUp = ({ email, username, password }) => async (dispatch) => {
  dispatch({ type: actions.AUTH_START });
  try {
    await firebase.auth().createUserWithEmailAndPassword(email, password);

    const user = firebase.auth().currentUser;

    await user.updateProfile({
      displayName: username,
    });

    await createUserReference(user);

    dispatch({ type: actions.AUTH_SUCCESS, payload: user });
  } catch (error) {
    dispatch({ type: actions.AUTH_FAILED, payload: error.message });
    showToast(true, null, error.message);
  }
  dispatch({ type: actions.AUTH_END });
};

// Sign in with email and password action creator
const signIn = ({ email, password }) => async (dispatch) => {
  dispatch({ type: actions.AUTH_START });
  try {
    await firebase.auth().signInWithEmailAndPassword(email, password);

    const user = firebase.auth().currentUser;

    dispatch({ type: actions.AUTH_SUCCESS, payload: user });
  } catch (error) {
    dispatch({ type: actions.AUTH_FAILED, payload: error.message });
    showToast(true, null, error.message);
  }
  dispatch({ type: actions.AUTH_END });
};

// Sign in with google action creator
const signInWithGoogle = () => async (dispatch) => {
  dispatch({ type: actions.AUTH_START });
  await firebase
    .auth()
    .signInWithPopup(new firebase.auth.GoogleAuthProvider())
    .then(async (response) => {
      dispatch({ type: actions.AUTH_SUCCESS, payload: response.user });

      await createUserReference(response.user);
    })
    .catch((error) => {
      dispatch({ type: actions.AUTH_FAILED, payload: error.message });
      showToast(true, null, error.message);
    });
  dispatch({ type: actions.AUTH_END });
};

// Sign in with facebook action creator
const signInWithFacebook = () => async (dispatch) => {
  dispatch({ type: actions.AUTH_START });
  await firebase
    .auth()
    .signInWithPopup(new firebase.auth.FacebookAuthProvider())
    .then(async (response) => {
      dispatch({ type: actions.AUTH_SUCCESS, payload: response.user });

      await createUserReference(response.user);
    })
    .catch((error) => {
      dispatch({ type: actions.AUTH_FAILED, payload: error.message });
      showToast(true, null, error.message);
    });
  dispatch({ type: actions.AUTH_END });
};

// Create user reference in databese
const createUserReference = async ({ uid, displayName, email, photoURL }) => {
  try {
    await firebase
      .database()
      .ref(`/users/${uid}/`)
      .set({ uid, displayName, email, photoURL });
  } catch (error) {
    showToast(true, null, error.message);
  }
};

// Logout action creator
const logout = () => async (dispatch) => {
  const currentUser = firebase.auth().currentUser;
  try {
    await firebase
      .database()
      .ref("presence")
      .child(currentUser.uid)
      .ref.remove();

    await firebase.auth().signOut();
  } catch (error) {
    dispatch({ type: actions.AUTH_FAILED, payload: error.message });
    showToast(true, null, error.message);
  }
};

// Password recovery action creator
const sendPasswordRecover = ({ email }) => async (dispatch) => {
  dispatch({ type: actions.AUTH_START });
  try {
    await firebase.auth().sendPasswordResetEmail(email);
    showToast(null, true, `Письмо успешно отправлено на ${email}`);
  } catch (error) {
    dispatch({ type: actions.AUTH_FAILED, payload: error.message });
    showToast(true, null, error.message);
  }
  dispatch({ type: actions.AUTH_END });
};

// Clean up messages
const cleanUp = () => (dispatch) => dispatch({ type: actions.CLEAN_UP });

export {
  checkCurrentUser,
  signUp,
  signIn,
  cleanUp,
  logout,
  sendPasswordRecover,
  signInWithGoogle,
  signInWithFacebook,
};
