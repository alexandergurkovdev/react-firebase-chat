import * as actions from "../actions/actionTypes";

const initialState = {
  loading: false,
  error: null,
  items: [],
};

const fetchUsersStart = (state) => {
  return {
    ...state,
    loading: true,
  };
};

const fetchUsersEnd = (state) => {
  return {
    ...state,
    loading: false,
  };
};

const fetchUsersFailed = (state, payload) => {
  return {
    ...state,
    error: payload,
  };
};

const fetchUsersSuccess = (state, payload) => {
  return {
    ...state,
    items: [...state.items, payload],
  };
};

const addStatusToUser = (state, { userId, connected = true }) => {
  const newUsers = [...state.items];

  if (newUsers.length) {
    const item = newUsers.find((user) => user.uid === userId);

    if (item) {
      connected === true ? (item.status = "online") : (item.status = "offline");
      return {
        ...state,
        items: newUsers,
      };
    }
  } else {
    return {
      ...state,
      items: newUsers,
    };
  }
};

const usersReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actions.FETCH_USERS_START:
      return fetchUsersStart(state);

    case actions.FETCH_USERS_END:
      return fetchUsersEnd(state);

    case actions.FETCH_USERS_FAILED:
      return fetchUsersFailed(state, payload);

    case actions.FETCH_USERS_SUCCESS:
      return fetchUsersSuccess(state, payload);

    case actions.ADD_STATUS_TO_USER:
      return addStatusToUser(state, payload);

    default:
      return state;
  }
};

export default usersReducer;
