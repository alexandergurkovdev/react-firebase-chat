import { toast } from "react-toastify";

export const showToast = (isError, isSuccess, message) => {
  if (isError) {
    toast.error(`${message}`, {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: 5000,
    });
  }

  if (isSuccess) {
    toast.success(`${message}`, {
      position: toast.POSITION.TOP_RIGHT,
      autoClose: 5000,
    });
  }
};
