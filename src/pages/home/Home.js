import React from "react";
import { connect } from "react-redux";
import { Jumbotron } from "react-bootstrap";

const Home = ({ activeChat }) => {
  if (activeChat) {
    return null;
  }

  return (
    <Jumbotron className="d-flex" style={{ height: "100%" }}>
      <h3 className="text-center m-auto">
        Выберите или создайте чат и начните общаться
      </h3>
    </Jumbotron>
  );
};

const mapStateToProps = ({ chat: { activeChat } }) => ({
  activeChat,
});

export default connect(mapStateToProps)(Home);
