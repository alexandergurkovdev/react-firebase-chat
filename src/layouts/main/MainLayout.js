import React, { useState } from "react";
import { Button } from "react-bootstrap";
import Sidebar from "../../containers/sidebar";

const MainLayout = ({ children }) => {
  const [chatSelected, setChatSelected] = useState(false);

  return (
    <div className={`main-layout d-flex ${chatSelected ? "active" : ""}`}>
      {chatSelected ? (
        <Button
          variant="primary"
          className="back-to-chats"
          onClick={() => setChatSelected(false)}
        >
          <i className="las la-arrow-left"></i>
        </Button>
      ) : null}

      <Sidebar setChatSelected={setChatSelected} />
      <div className="main-layout__inner">{children}</div>
    </div>
  );
};

export default MainLayout;
