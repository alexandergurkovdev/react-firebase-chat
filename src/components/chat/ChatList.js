import React, { useEffect } from "react";
import { useHistory } from "react-router-dom";
import { ListGroup, Image } from "react-bootstrap";

const ChatList = ({
  chatList,
  setActiveChat,
  activeChat,
  users,
  currentUserName,
}) => {
  const history = useHistory();

  useEffect(() => {
    if (activeChat) {
      history.push(`/chat/${activeChat}`);
    }
  });

  let status = "offline";
  let photoURL = "";

  const userInfo = (name) => {
    const user = users.find((user) => user.displayName === name);
    if (user) {
      status = user.status;
      photoURL = user.photoURL;
    }
  };

  return (
    <ListGroup as="ul" className="users-list">
      {chatList.map(({ id, name, creator }) => {
        if (creator === currentUserName) {
          userInfo(name);
        } else {
          userInfo(creator);
        }

        return (
          <ListGroup.Item
            as="li"
            key={id}
            className={status}
            onClick={() => setActiveChat(id)}
            active={activeChat && activeChat.indexOf(id) > -1}
          >
            {photoURL ? <Image src={photoURL} roundedCircle /> : null}
            {creator === currentUserName ? name : creator}
            <span className="status" />
          </ListGroup.Item>
        );
      })}
    </ListGroup>
  );
};

export default ChatList;
