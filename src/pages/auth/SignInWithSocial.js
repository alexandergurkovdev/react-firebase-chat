import React from "react";
import { Button } from "react-bootstrap";

const SignInWithSocial = ({ signInWithFacebook, signInWithGoogle }) => {
  return (
    <div className="login-with-social">
      <p className="login-with-social__text text-center pt-3">
        Вход с помощью соцсетей
      </p>
      <div className="login-with-social__buttons d-flex">
        <Button
          variant="primary"
          className="mr-3"
          onClick={async () => await signInWithFacebook()}
        >
          <i className="lab la-facebook-f" />
        </Button>
        <Button
          variant="danger"
          className="ms-3"
          onClick={async () => await signInWithGoogle()}
        >
          <i className="lab la-google" />
        </Button>
      </div>
    </div>
  );
};

export default SignInWithSocial;
