import React, { useEffect, useRef } from "react";
import ChatMessage from "./ChatMessage";

const ChatMessages = ({ messages, currentUserId, onDeleteMessage }) => {
  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  return (
    <div className="chat-messages">
      {messages.map((message) => {
        return (
          <ChatMessage
            key={message.id}
            message={message}
            currentUserId={currentUserId}
            onDeleteMessage={onDeleteMessage}
          />
        );
      })}
      <div ref={messagesEndRef} />
    </div>
  );
};

export default ChatMessages;
