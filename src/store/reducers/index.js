import { combineReducers } from "redux";
import authReducer from "./authReducer";
import chatReducer from "./chatReducer";
import usersReducer from "./usersReducer";

export default combineReducers({
  auth: authReducer,
  chat: chatReducer,
  users: usersReducer,
});
