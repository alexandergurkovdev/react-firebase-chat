import firebase from "firebase/app";
import { v4 as uuidv4 } from "uuid";
import * as actions from "./actionTypes";
import { showToast } from "../../utils/toast";

// Set active chat action creator
const setActiveChat = (uid) => async (dispatch) => {
  dispatch({
    type: actions.SET_ACTIVE_CHAT,
    payload: uid,
  });

  try {
    const res = await firebase
      .firestore()
      .collection("messages")
      .doc(uid)
      .get();

    if (res && res.data()) {
      dispatch({
        type: actions.FETCH_MESSAGES_SUCCESS,
        payload: res.data().messages,
      });
    } else {
      dispatch({
        type: actions.FETCH_MESSAGES_SUCCESS,
        payload: [],
      });
    }
  } catch (error) {
    showToast(true, null, error.message);
  }
};

// Add chat action creator
const addChat = (chatId, currentUserId, user, currentUserName) => async (
  dispatch
) => {
  dispatch({ type: actions.ADD_CHAT_START });
  try {
    const chat = {
      id: chatId,
      name: user.displayName,
      creator: currentUserName,
      users: [currentUserId, user.uid],
    };
    const res = await firebase
      .firestore()
      .collection("chats")
      .doc(chatId)
      .get();

    if (res && res.data()) {
      showToast(true, null, "Такой чат уже существует");
    } else {
      await firebase.firestore().collection("chats").doc(chatId).set(chat);
    }

    dispatch({ type: actions.ADD_CHAT_END });
  } catch (error) {
    dispatch({ type: actions.ADD_CHAT_ERROR, payload: error.message });
    showToast(true, null, error.message);
  }
  dispatch({ type: actions.ADD_CHAT_END });
};

// Get chats action creator
const getChats = (currentUserId) => (dispatch) => {
  try {
    firebase
      .firestore()
      .collection("chats")
      .where("users", "array-contains", currentUserId)
      .onSnapshot((querySnapshot) => {
        querySnapshot.forEach((doc) => {
          dispatch({ type: actions.FETCH_CHATS_SUCCESS, payload: doc.data() });
        });
      });
  } catch (error) {
    dispatch({ type: actions.FETCH_CHATS_FAILED, payload: error.message });
    showToast(true, null, error.message);
  }
};

const clearChats = () => (dispatch) => {
  dispatch({ type: actions.CLEAR_CHATS });
};

// Send message actoin creator
const sendMessage = (message, messageType, chatId, filePath) => async (
  dispatch
) => {
  const currentUser = firebase.auth().currentUser;

  const newMessage = {
    id: uuidv4(),
    message,
    messageType,
    timestamp: firebase.firestore.Timestamp.fromDate(new Date()),
    filePath,
    sender: {
      uid: currentUser.uid,
      name: currentUser.displayName,
    },
  };

  try {
    const res = await firebase
      .firestore()
      .collection("messages")
      .doc(chatId)
      .get();

    if (!res.data()) {
      await firebase
        .firestore()
        .collection("messages")
        .doc(chatId)
        .set({
          messages: [newMessage],
          users: chatId.split("|"),
        });
    } else {
      await firebase
        .firestore()
        .collection("messages")
        .doc(chatId)
        .update({
          messages: [...res.data().messages, newMessage],
          users: chatId.split("|"),
        });
    }
  } catch (error) {
    dispatch({ type: actions.SEND_MESSAGE_ERROR, payload: error.message });
    showToast(true, null, error.message);
  }
};

const listenMessages = (chatId) => async (dispatch) => {
  await firebase
    .firestore()
    .collection("messages")
    .doc(chatId)
    .onSnapshot((doc) => {
      if (doc.data()) {
        dispatch({
          type: actions.FETCH_MESSAGES_SUCCESS,
          payload: doc.data().messages,
        });
      }
    });
};

const deleteMessage = (chatId, messageId, filePath) => async (dispatch) => {
  try {
    const res = await firebase
      .firestore()
      .collection("messages")
      .doc(chatId)
      .get();

    if (filePath) {
      await firebase
        .storage()
        .ref()
        .child(filePath)
        .delete()
        .then(() => {
          showToast(null, true, "Файл успешно удалён");
        })
        .catch((error) => {
          showToast(true, null, error.message);
        });
    }

    const newMessages = res
      .data()
      .messages.filter((message) => message.id !== messageId);

    await firebase
      .firestore()
      .collection("messages")
      .doc(chatId)
      .set({
        messages: newMessages,
        users: chatId.split("|"),
      });
  } catch (error) {
    showToast(true, null, error.message);
  }
};

export {
  setActiveChat,
  addChat,
  getChats,
  listenMessages,
  sendMessage,
  deleteMessage,
  clearChats,
};
