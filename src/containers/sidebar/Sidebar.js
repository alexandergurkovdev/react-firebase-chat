import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { OverlayTrigger, Tooltip, Button } from "react-bootstrap";
import Dialog from "../../components/ui/dialog";
import UsersList from "../../components/users-list";
import { ChatList } from "../../components/chat";
import Loader from "../../components/ui/loader";
import { logout } from "../../store/actions/authActions";
import { getUsers, listenUerStatuses } from "../../store/actions/usersActions";
import { clearChats } from "../../store/actions/chatActions";
import {
  setActiveChat,
  addChat,
  getChats,
} from "../../store/actions/chatActions";

const Sidebar = ({
  currentUserId,
  currentUserName,
  setChatSelected,
  setActiveChat,
  addChat,
  clearChats,
  getUsers,
  listenUerStatuses,
  getChats,
  usersLoading,
  users,
  chats,
  activeChat,
  logout,
}) => {
  const [modalShow, setModalShow] = useState(false);
  const [selectedUser, setSelectedUser] = useState(null);
  const [generateChatId, setGenerateChatId] = useState(false);

  const onSetModalShow = (value) => {
    setModalShow(value);

    if (value === false) {
      setSelectedUser(null);
      setGenerateChatId(null);
    }
  };

  const generateId = (value) => {
    if (value.uid < currentUserId) {
      return value.uid + "|" + currentUserId;
    } else {
      return currentUserId + "|" + value.uid;
    }
  };

  const setActiveChatToStore = (value) => {
    setActiveChat(value);
    setChatSelected(true);
  };

  const setSelectedUserHandler = (value) => {
    const chatId = generateId(value);
    setSelectedUser(value);
    setGenerateChatId(chatId);
  };

  const addChatHandler = async () => {
    setActiveChat(generateChatId);
    await addChat(generateChatId, currentUserId, selectedUser, currentUserName);
    setChatSelected(true);
  };

  useEffect(() => {
    async function fetchData() {
      await getUsers();
      await listenUerStatuses();
    }
    fetchData();
    getChats(currentUserId);
  }, []);

  useEffect(
    () => () => {
      clearChats();
    },
    []
  );

  return (
    <div className="sidebar d-flex flex-column">
      {!chats.length && usersLoading ? <Loader /> : null}

      <ChatList
        chatList={chats}
        setActiveChat={setActiveChatToStore}
        activeChat={activeChat}
        users={users}
        currentUserName={currentUserName}
      />

      <div className="sidebar__footer d-flex mt-auto">
        <Button variant="danger" onClick={logout}>
          Выйти
        </Button>

        <Dialog
          title="Список пользователей"
          description="Выберите пользователя, чтобы начать чат"
          handleClose={onSetModalShow}
          addChat={addChatHandler}
          show={modalShow}
          selectedUser={selectedUser}
          generateChatId={generateChatId}
        >
          <UsersList
            users={users}
            selectedUser={selectedUser}
            setSelectedUser={setSelectedUserHandler}
          />
        </Dialog>

        <OverlayTrigger
          placement="top"
          overlay={<Tooltip id="top">Добавить чат</Tooltip>}
        >
          <Button
            className="ml-3"
            variant="primary"
            onClick={() => setModalShow(true)}
          >
            <i className="las la-plus" />
          </Button>
        </OverlayTrigger>
      </div>
    </div>
  );
};

const mapStateToProps = ({
  auth: {
    currentUser: { uid, displayName },
  },
  users: { items, loading },
  chat: { chats, activeChat },
}) => ({
  currentUserId: uid,
  currentUserName: displayName,
  users: items,
  usersLoading: loading,
  chats,
  activeChat,
});

const mapDispatchToProps = {
  logout,
  getUsers,
  listenUerStatuses,
  setActiveChat,
  addChat,
  getChats,
  clearChats,
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
