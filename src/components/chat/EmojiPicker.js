import React, { Fragment } from "react";
import { Picker } from "emoji-mart";
import OutsideClickHandler from "react-outside-click-handler";
import "emoji-mart/css/emoji-mart.css";

const EmojiPicker = ({ emojiShow, onEmojiSelect, onEmojiToggler }) => {
  return (
    <Fragment>
      {emojiShow ? (
        <OutsideClickHandler
          onOutsideClick={() => {
            onEmojiToggler(false);
          }}
        >
          <Picker
            onSelect={(emoji) => onEmojiSelect(emoji)}
            exclude={["flags", "foods", "activity", "symbols"]}
            set="google"
          />
        </OutsideClickHandler>
      ) : null}
    </Fragment>
  );
};

export default EmojiPicker;
