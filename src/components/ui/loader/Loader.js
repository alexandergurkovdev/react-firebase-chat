import React from "react";
import { Spinner } from "react-bootstrap";

const Loader = ({ theme = "primary", bgShow }) => {
  return (
    <div
      className={`spinner-wrapper ${
        bgShow ? "spinner-wrapper__bacground" : ""
      }`}
    >
      <Spinner animation="border" role="status" variant={theme}>
        <span className="sr-only">Загрузка...</span>
      </Spinner>
    </div>
  );
};

export default Loader;
