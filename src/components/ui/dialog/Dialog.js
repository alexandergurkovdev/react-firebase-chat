import React from "react";
import { useHistory } from "react-router-dom";
import { Modal, Button } from "react-bootstrap";

const Dialog = ({
  title,
  description,
  handleClose,
  children,
  show,
  selectedUser,
  generateChatId,
  addChat,
}) => {
  const history = useHistory();

  return (
    <Modal show={show} onHide={() => handleClose(false)}>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
        <p className="mb-0">{description}</p>
      </Modal.Header>
      <Modal.Body>{children}</Modal.Body>
      <Modal.Footer>
        {selectedUser ? (
          <Button
            variant="primary"
            onClick={async () => {
              await addChat();
              history.push(`/chat/${generateChatId}`);
              handleClose(false);
            }}
          >
            Начать чат
          </Button>
        ) : null}
        <Button variant="secondary" onClick={() => handleClose(false)}>
          Закрыть
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default Dialog;
