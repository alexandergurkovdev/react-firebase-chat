import * as actions from "../actions/actionTypes";

const initialState = {
  loading: false,
  error: null,
  currentUser: null,
};

// Auth
const authStart = (state) => {
  return {
    ...state,
    loading: true,
  };
};
const authEnd = (state) => {
  return {
    ...state,
    loading: false,
  };
};
const authFailed = (state, payload) => {
  return {
    ...state,
    error: payload,
  };
};
const authSuccess = (state, payload) => {
  return {
    ...state,
    currentUser: payload,
  };
};

const cleanUp = (state) => {
  return {
    ...state,
    error: null,
    loading: false,
  };
};

const authReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actions.CHECK_CURRENT_USER:
      return {
        loading: false,
        error: null,
        currentUser: payload,
      };

    case actions.CLEAN_UP:
      return cleanUp(state);

    case actions.AUTH_START:
      return authStart(state);

    case actions.AUTH_END:
      return authEnd(state);

    case actions.AUTH_FAILED:
      return authFailed(state, payload);

    case actions.AUTH_SUCCESS:
      return authSuccess(state, payload);

    default:
      return state;
  }
};

export default authReducer;
