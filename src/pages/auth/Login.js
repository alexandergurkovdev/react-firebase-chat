import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";
import { Form, Col, Button } from "react-bootstrap";
import RouterLink from "../../components/ui/router-link";
import Loader from "../../components/ui/loader";
import SignInWithSocial from "./SignInWithSocial";
import {
  signIn,
  cleanUp,
  signInWithFacebook,
  signInWithGoogle,
} from "../../store/actions/authActions";

const LoginSchema = Yup.object().shape({
  email: Yup.string()
    .email("Введите корректный email")
    .required("Поле email обязательно к заполнению"),
  password: Yup.string()
    .required("Поле пароль обязательно к заполнению")
    .min(6, "Минимальная длина пароля 6 символов"),
});

const Login = ({
  loading,
  signIn,
  cleanUp,
  signInWithFacebook,
  signInWithGoogle,
}) => {
  useEffect(() => {
    return () => {
      cleanUp();
    };
  }, [cleanUp]);

  return (
    <Formik
      initialValues={{
        email: "",
        password: "",
      }}
      validationSchema={LoginSchema}
      onSubmit={async (values, { setSubmitting }) => {
        await signIn(values);
        setSubmitting(false);
      }}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        errors,
        isSubmiting,
        isValid,
      }) => (
        <Form className="auth-form" noValidate onSubmit={handleSubmit}>
          {loading ? <Loader bgShow /> : null}
          <Form.Row>
            <Form.Group as={Col} md="12" controlId="validationFormik01">
              <Form.Control
                type="email"
                name="email"
                placeholder="Email"
                value={values.email}
                onChange={handleChange}
                isInvalid={!!errors.email}
              />
              <Form.Control.Feedback type="invalid">
                {errors.email}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationFormik03">
              <Form.Control
                type="password"
                name="password"
                placeholder="Пароль"
                value={values.password}
                onChange={handleChange}
                isInvalid={!!errors.password}
              />
              <Form.Control.Feedback type="invalid">
                {errors.password}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Button type="submit" disabled={!isValid || isSubmiting}>
                Вход
              </Button>
              <RouterLink link="/sign-up" label="Нет аккаунта? ">
                Зарегистрироваться
              </RouterLink>
              <RouterLink link="/recovery" label="Забыли пароль? ">
                Восстановить
              </RouterLink>
            </Form.Group>
          </Form.Row>

          <SignInWithSocial
            signInWithFacebook={signInWithFacebook}
            signInWithGoogle={signInWithGoogle}
          />
        </Form>
      )}
    </Formik>
  );
};

const mapStateToProps = ({ auth: { loading } }) => ({
  loading,
});

const mapDispatchToProps = {
  signIn,
  cleanUp,
  signInWithFacebook,
  signInWithGoogle,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
