import React from "react";
import Linkify from "react-linkify";
import Moment from "react-moment";
import ReactAudioPlayer from "react-audio-player";

const ChatMessage = ({
  message: { id, message, messageType, sender, timestamp, filePath },
  currentUserId,
  onDeleteMessage,
}) => {
  const linkDecorator = (href, text, key) => (
    <a href={href} key={key} target="_blank" rel="noopener noreferrer">
      {text}
    </a>
  );

  const date = () => {
    return timestamp.seconds * 1000 + timestamp.nanoseconds / 1000000;
  };

  return (
    <div
      className={`chat-message ${
        sender.uid === currentUserId ? "chat-message--my" : ""
      }`}
    >
      {sender.uid === currentUserId ? (
        <button
          className="chat-message_delete"
          onClick={() => onDeleteMessage(id, filePath)}
        >
          <i className="las la-trash-alt" />
        </button>
      ) : null}
      {messageType === "text" ? (
        <div>
          <Linkify componentDecorator={linkDecorator}>{message}</Linkify>
        </div>
      ) : (
        <ReactAudioPlayer src={message} controls />
      )}

      <span className="chat-message__date">
        <Moment fromNow date={date()} />
      </span>
    </div>
  );
};

export default ChatMessage;
