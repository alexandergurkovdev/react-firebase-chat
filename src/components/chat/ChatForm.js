import React from "react";
import { Form, Button, OverlayTrigger, Tooltip } from "react-bootstrap";
import TextareaAutosize from "react-textarea-autosize";
import EmojiPicker from "./EmojiPicker";

const ChatForm = ({
  message,
  messageType,
  record,
  emojiShow,
  onTypeMessage,
  sendMessage,
  onStartRecording,
  onStopRecording,
  onEmojiToggler,
  onEmojiSelect,
}) => {
  return (
    <Form
      noValidate
      className="chat-form"
      onSubmit={async (e) => {
        e.preventDefault();
        await sendMessage();
      }}
    >
      <OverlayTrigger
        placement="top"
        overlay={
          <Tooltip id="faq-tooltip" className="faq-tooltip">
            Клавиша <strong>Enter</strong> - отправка сообщения <br />
            Комбинация <strong>Shift + Enter</strong> - перенос строки <br />
            Показать смайлики - <strong>Tab</strong>
          </Tooltip>
        }
      >
        <Button variant="secondary" className="mr-3">
          <i className="las la-question-circle" />
        </Button>
      </OverlayTrigger>

      <TextareaAutosize
        value={message}
        className="form-control"
        disabled={record}
        type="text"
        placeholder="Введите сообщение..."
        onKeyPress={async (e) => {
          if (e.key === "Enter" && message) {
            if (!e.shiftKey) {
              await sendMessage();
            }
          }
        }}
        onChange={(e) => {
          onTypeMessage(e);
        }}
      />

      <Button
        variant="success"
        className="emoji-toggler"
        size="sm"
        onClick={() => onEmojiToggler(true)}
      >
        <i className="las la-smile" />
      </Button>

      <EmojiPicker
        emojiShow={emojiShow}
        onEmojiSelect={onEmojiSelect}
        onEmojiToggler={onEmojiToggler}
      />

      {messageType === "voice" && !message ? (
        <Button
          variant={record ? "danger" : "primary"}
          onClick={() => {
            if (!record) {
              onStartRecording();
            } else {
              onStopRecording();
            }
          }}
        >
          <i className="las la-microphone" />
        </Button>
      ) : (
        <Button type="submit">
          <i className="las la-paper-plane" />
        </Button>
      )}
    </Form>
  );
};

export default ChatForm;
