import MainLayout from "./main/MainLayout";
import AuthLayout from "./auth/AuthLayout";

export { MainLayout, AuthLayout };
