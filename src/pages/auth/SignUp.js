import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";
import { Form, Col, Button } from "react-bootstrap";
import RouterLink from "../../components/ui/router-link";
import Loader from "../../components/ui/loader";
import SignInWithSocial from "./SignInWithSocial";
import {
  signUp,
  cleanUp,
  signInWithFacebook,
  signInWithGoogle,
} from "../../store/actions/authActions";

const SignUpSchema = Yup.object().shape({
  email: Yup.string()
    .email("Введите корректный email")
    .required("Поле email обязательно к заполнению"),
  username: Yup.string().required("Поле username обязательно к заполнению"),
  password: Yup.string()
    .required("Поле пароль обязательно к заполнению")
    .min(6, "Минимальная длина пароля 6 символов"),
});

const SignUp = ({
  loading,
  signUp,
  cleanUp,
  signInWithFacebook,
  signInWithGoogle,
}) => {
  useEffect(() => {
    return () => {
      cleanUp();
    };
  }, [cleanUp]);

  return (
    <Formik
      initialValues={{
        email: "",
        username: "",
        password: "",
      }}
      validationSchema={SignUpSchema}
      onSubmit={async (values, { setSubmitting }) => {
        await signUp(values);
        setSubmitting(false);
      }}
    >
      {({
        handleSubmit,
        handleChange,
        values,
        errors,
        isSubmiting,
        isValid,
      }) => (
        <Form className="auth-form" noValidate onSubmit={handleSubmit}>
          {loading ? <Loader bgShow /> : null}
          <Form.Row>
            <Form.Group as={Col} md="12" controlId="validationFormik01">
              <Form.Control
                type="email"
                name="email"
                placeholder="Email"
                value={values.email}
                onChange={handleChange}
                isInvalid={!!errors.email}
              />
              <Form.Control.Feedback type="invalid">
                {errors.email}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationFormik02">
              <Form.Control
                type="text"
                name="username"
                placeholder="Никнейм"
                value={values.username}
                onChange={handleChange}
                isInvalid={!!errors.username}
              />
              <Form.Control.Feedback type="invalid">
                {errors.username}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="12" controlId="validationFormik03">
              <Form.Control
                type="password"
                name="password"
                placeholder="Пароль"
                value={values.password}
                onChange={handleChange}
                isInvalid={!!errors.password}
              />
              <Form.Control.Feedback type="invalid">
                {errors.password}
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group as={Col} md="12">
              <Button type="submit" disabled={!isValid || isSubmiting}>
                Регистрация
              </Button>
              <RouterLink link="/login" label="Есть аккаунт? ">
                Войти
              </RouterLink>
            </Form.Group>
          </Form.Row>

          <SignInWithSocial
            signInWithFacebook={signInWithFacebook}
            signInWithGoogle={signInWithGoogle}
          />
        </Form>
      )}
    </Formik>
  );
};

const mapStateToProps = ({ auth: { loading } }) => ({
  loading,
});

const mapDispatchToProps = {
  signUp,
  cleanUp,
  signInWithFacebook,
  signInWithGoogle,
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
