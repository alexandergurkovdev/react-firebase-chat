import * as actions from "../actions/actionTypes";

const initialState = {
  activeChat: null,
  loading: false,
  error: null,
  chats: [],
  messages: [],
};

const setActiveChat = (state, payload) => {
  return {
    ...state,
    activeChat: payload,
  };
};

const addChatStart = (state) => {
  return {
    ...state,
    loading: true,
  };
};
const addChatEnd = (state) => {
  return {
    ...state,
    loading: false,
  };
};
const addChatError = (state, payload) => {
  return {
    ...state,
    error: payload,
  };
};

const fetchChatsFailed = (state, payload) => {
  return {
    ...state,
    error: payload,
  };
};
const fetchChatsSuccess = (state, payload) => {
  return {
    ...state,
    chats: [...state.chats, payload],
  };
};
const clearChats = (state) => {
  return {
    ...state,
    chats: [],
  };
};
const fetchMessagesSuccess = (state, payload) => {
  return {
    ...state,
    messages: payload,
  };
};

const chatReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case actions.SET_ACTIVE_CHAT:
      return setActiveChat(state, payload);

    case actions.ADD_CHAT_START:
      return addChatStart(state);

    case actions.ADD_CHAT_END:
      return addChatEnd(state);

    case actions.ADD_CHAT_ERROR:
      return addChatError(state, payload);

    case actions.FETCH_CHATS_FAILED:
      return fetchChatsFailed(state, payload);

    case actions.FETCH_CHATS_SUCCESS:
      return fetchChatsSuccess(state, payload);

    case actions.CLEAR_CHATS:
      return clearChats(state, payload);

    case actions.FETCH_MESSAGES_SUCCESS:
      return fetchMessagesSuccess(state, payload);

    default:
      return state;
  }
};

export default chatReducer;
