import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import firebase from "firebase/app";
import { v4 as uuidv4 } from "uuid";
import { ReactMic } from "react-mic";
import { ProgressBar } from "react-bootstrap";
import {
  sendMessage,
  listenMessages,
  deleteMessage,
} from "../../store/actions/chatActions";
import { ChatMessages, ChatForm } from "../../components/chat";
import { showToast } from "../../utils/toast";

const Chat = ({
  messages,
  currentUserId,
  activeChat,
  sendMessage,
  deleteMessage,
  listenMessages,
}) => {
  const [message, setMessage] = useState("");
  const [messageType, setMessageType] = useState("voice");
  const [record, setRecord] = useState(false);
  const [uploadProgress, setUploadProgress] = useState(0);
  const [emojiShow, setEmojiShow] = useState(false);

  const onTypeMessage = (e) => {
    if (e.target.value.trim() !== "") {
      setMessage(e.target.value);
      setMessageType("text");
    } else {
      setMessage("");
      setMessageType("voice");
    }
  };

  const onEmojiToggler = (value) => {
    setEmojiShow(value);
  };

  const onEmojiSelect = (emoji) => {
    if (message) {
      setMessage(message + emoji.native);
    } else {
      setMessage(emoji.native);
    }
  };

  const handleKeyDown = (e) => {
    switch (e.key) {
      case "Tab":
        setEmojiShow(true);
        break;

      default:
        break;
    }
  };

  const getVoiceMessagePath = () => {
    return `chats/${activeChat}/${uuidv4()}`;
  };

  const onStartRecording = () => {
    setRecord(true);
  };

  const onStopRecording = () => {
    setRecord(false);
  };

  const onStop = async (recordedBlob) => {
    let metadata = {
      contentType: "audio/wav",
    };
    let filePath = getVoiceMessagePath();
    let uploadTask = firebase
      .storage()
      .ref()
      .child(filePath)
      .put(recordedBlob.blob, metadata);

    uploadTask.on(
      "state_changed",
      (snapshot) => {
        setUploadProgress(
          (snapshot.bytesTransferred / snapshot.totalBytes) * 100
        );
      },
      (error) => {
        showToast(true, null, error.message);
      },
      () => {
        setUploadProgress(100);

        uploadTask.snapshot.ref.getDownloadURL().then(async (url) => {
          await sendMessage(url, messageType, activeChat, filePath);

          if (url) {
            setUploadProgress(0);
          }
        });
      }
    );
  };

  const onSendMessage = async () => {
    await sendMessage(message, messageType, activeChat, null);
    setMessage("");
    setMessageType("voice");
  };

  const onDeleteMessage = async (messageId, filePath) => {
    await deleteMessage(activeChat, messageId, filePath);
  };

  useEffect(() => {
    listenMessages(activeChat);

    document.addEventListener("keydown", handleKeyDown);
  }, []);

  useEffect(
    () => () => {
      document.removeEventListener("keydown", handleKeyDown);
    },
    []
  );

  return (
    <div className="chat">
      {uploadProgress > 0 ? (
        <ProgressBar variant="success" now={uploadProgress} />
      ) : null}

      <ChatMessages
        messages={messages}
        currentUserId={currentUserId}
        onDeleteMessage={onDeleteMessage}
      />

      <ReactMic
        record={record}
        onStop={onStop}
        mimeType="audio/wav"
        style={{ display: "none" }}
      />
      <ChatForm
        message={message}
        messageType={messageType}
        record={record}
        emojiShow={emojiShow}
        onTypeMessage={onTypeMessage}
        sendMessage={onSendMessage}
        onStartRecording={onStartRecording}
        onStopRecording={onStopRecording}
        onEmojiToggler={onEmojiToggler}
        onEmojiSelect={onEmojiSelect}
      />
    </div>
  );
};

const mapStateToProps = ({
  chat: { activeChat, messages },
  auth: {
    currentUser: { uid },
  },
}) => ({
  activeChat,
  messages,
  currentUserId: uid,
});

const mapDispatchToProps = {
  sendMessage,
  listenMessages,
  deleteMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
