import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import App from "./App";
import Loader from "./components/ui/loader";
import "react-toastify/dist/ReactToastify.css";
import "./scss/app.scss";

// firebase import
import "./firebase";

const root = document.getElementById("root");

ReactDOM.render(<Loader />, root);

window.firebase.auth().onAuthStateChanged(() => {
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <App />
      </Router>
    </Provider>,
    root
  );
});
