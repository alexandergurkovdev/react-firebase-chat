import React from "react";
import { ListGroup, Image } from "react-bootstrap";

const UsersList = ({ users, setSelectedUser, selectedUser }) => {
  return (
    <ListGroup as="ul" className="users-list">
      {users.map(({ uid, displayName, status, photoURL }) => {
        return (
          <ListGroup.Item
            as="li"
            key={uid}
            className={status}
            onClick={() => setSelectedUser({ uid, displayName })}
            active={selectedUser && selectedUser.uid === uid}
          >
            {photoURL ? <Image src={photoURL} roundedCircle /> : null}

            {displayName}
            <span className="status" />
          </ListGroup.Item>
        );
      })}
    </ListGroup>
  );
};

export default UsersList;
